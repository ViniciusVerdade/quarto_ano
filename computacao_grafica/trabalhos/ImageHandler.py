from PIL import Image
import random
import math as math
import numpy as np

class ImageHandler:
    def __init__(self, filename):
        self.filename = filename
        self.img = Image.open(filename)
        self.matriz_de_uns = np.ones([3,3])
        self.filtro_laplace = self.setFiltroLaplace()
        self.filtro_sobel = self.setFiltroSobel()
        self.alfas = None
        self.cossenosX = None
        self.cossenosY = None
    
    def convolucao(self, img, filtro):
        """
        Matriz que passa o filtro desejado\n
        @param img = imagem que será passado o filtro\n
        @param filtro = filtro que passará como matriz de convolução\n
        @return = imagem com o filtro passado
        """
        new_img = Image.new("RGB", (img.width, img.height))
        new_img_pixels = new_img.load()
        pixels_array = img.load()

        width = len(filtro[:,0])
        height= len(filtro[0,:])
        #print("width: " + str(width))
        #input("height: " + str(height))
        offset_width = int(width/2 - 0.5)
        offset_height = int(height/2 - 0.5) 
        for i in range(img.width):
            for j in range(img.height):
                xi = i - offset_width
                xf = i + offset_width
                yi = j - offset_height
                yf = j + offset_height
                if (xi < 0 or yi < 0 or xf >= img.width or yf >= img.height):
                    continue
                value = np.zeros([3,1])
                for m in range(xi, xf+1):
                    #input("m: " + str(m))
                    for n in range(yi, yf+1):
                        pixel = pixels_array[m,n]
                        value[0] += pixel[0] * filtro[m-xi, n-yi]
                        value[1] += pixel[1] * filtro[m-xi, n-yi]
                        value[2] += pixel[2] * filtro[m-xi, n-yi]
                new_img_pixels[i,j] = (value[0], value[1], value[2])
        return new_img
    

    def filtragemLaplace(self, mostrar_imagem = 0):
        imagem_filtrada_por_laplace = self.convolucao(self.img, self.filtro_laplace)
        if mostrar_imagem == 1:
            imagem_filtrada_por_laplace.show()
        
    def filtragemSobel(self, mostrar_imagem = 0):
        imagem_filtrada_por_sobel = self.convolucao(self.img, self.filtro_sobel)
        if mostrar_imagem == 1:
            imagem_filtrada_por_sobel.show()

    def mostraImagemEnviada(self):
        self.img.show()
    
    def setFiltroLaplace(self):
        filtro_laplace = np.zeros([3,3])
        laplace  = [[0, -1, 0], [-1, 4, -1] ,[0, -1, 0]]
        
        filtro_laplace += laplace
        
        return filtro_laplace

    def setFiltroSobel(self,orientacao = 0):
        """
        Filtro de sobel\n
        @param img = 0 para horizontal e 1 para vertical
        @return = filtro setado
        """
        filtro_sobel = np.zeros([3,3])
        sobel_horizontal  = [[-1, -2, -1], [0, 0, 0] ,[1, 2, 1]]
        filtro_sobel += sobel_horizontal
        
        if(orientacao == 1):
            filtro_sobel = np.transpose(filtro_sobel)
        return filtro_sobel

    def filtragemButterworthComDCT(self,mostrar_imagem = 0):
        imagem_dominio_frequencia = Image.new("RGB",(self.img.width,self.img.height))
        print("Calculando as constantes...")
        self.calculandoConstantes(self.img.width,self.img.height)
        print("Constantes calculadas!")

        #matriz_dominio_frequencia = [ [ 0 for y in range( imagem_dominio_frequencia.width ) ] for x in range( imagem_dominio_frequencia.height ) ] 
        print("Iniciando a transformada...")
        matriz_dominio_frequencia = self.transformadaCossenoDiscreta(imagem_dominio_frequencia)
        print("Fim da transformada!")
        #imagem_dominio_frequencia = self.matrizParaImagem(matriz_dominio_frequencia)
        #imagem_dominio_frequencia.save("./imagens_resultado/transformada.jpg")
        matriz_passa_baixa = self.filtroPassaBaixaIdeal(matriz_dominio_frequencia, 20)
        matriz_passa_alta = self.filtroPassaAltaIdeal(matriz_dominio_frequencia, 5)
        print("Iniciando a transformada inversa normal...")
        matriz_imagem_normal = self.transformadaInversaCossenoDiscreta(matriz_dominio_frequencia)
        print("Fim da transformada!")
        print("\nIniciando a transformada inversa pós filtro passa baixa...")
        matriz_imagem_passa_baixa = self.transformadaInversaCossenoDiscreta(matriz_passa_baixa)
        print("Fim da transformada!")
        print("\nIniciando a transformada inversa pós filtro passa alta...")
        matriz_imagem_passa_alta = self.transformadaInversaCossenoDiscreta(matriz_passa_alta)
        print("Fim da transformada!")

        print("Iniciando a transformação das matrizes para imagens...")
        imagem_normal = self.matrizParaImagem(matriz_imagem_normal)
        imagem_passa_baixa = self.matrizParaImagem(matriz_imagem_passa_baixa)
        imagem_passa_alta = self.matrizParaImagem(matriz_imagem_passa_alta)
        
        imagem_normal.save("./imagens_resultado/transformadaInversa.jpg")
        imagem_passa_baixa.save("./imagens_resultado/transformadaInversa_passabaixa.jpg")
        imagem_passa_alta.save("./imagens_resultado/transformadaInversa_passaalta.jpg")
        print("Imagem salva!")
        #matriz_imagem_normal = self.transformadaInversaCossenoDiscreta(matriz_dominio_frequencia)
        #for width in range(imagem_dominio_frequencia.width):
        #    for height in range(imagem_dominio_frequencia.height):
        #        input("pixels da imagem_dominio_frequencia: "+str(pixel[width,height])) 
        # TODOS OS PIXELS AQUI SÃO (0,0,0)

    def filtroPassaBaixaIdeal(self,matriz_imagem,D_zero):
        print('Calculando filtro passa baixa...')
        width, height = len(matriz_imagem), len(matriz_imagem[0])
        D_zero_pow = math.pow(D_zero,2)
        imagem_saida = [[ [0, 0, 0] for i in range(height)] for i in range(width)]

        #print("matriz_imagem.shape"+str(matriz_imagem))
        #print("imagem_saida.shape"+str(imagem_saida))

        #loop de passagem
        for x in range(width):
            for y in range(height):
                distancia = self.distanciaQuadrada(0, 0, x, y)
                if distancia <= D_zero_pow:
                    imagem_saida[x][y] = matriz_imagem[x][y]
                    #print("matriz_imagem[x][y]: "+ str(matriz_imagem[x][y]))
                    #input("imagem_saida[x][y]: "+ str(imagem_saida[x][y]))

        return imagem_saida

    def filtroPassaAltaIdeal(self,matriz_imagem,D_zero):
        print('Calculando filtro passa alta...')
        width, height = len(matriz_imagem), len(matriz_imagem[0])
        D_zero_pow = math.pow(D_zero,2)
        imagem_saida = [[ [0, 0, 0] for i in range(height)] for i in range(width)]
        
        #loop de passagem
        for x in range(width):
            for y in range(height):
                distancia = self.distanciaQuadrada(0, 0, x, y)
                if distancia >= D_zero_pow:
                    imagem_saida[x][y] = matriz_imagem[x][y]

        return imagem_saida

    def transformadaCossenoDiscreta(self, imagem_freq):
        #all_pixels_freq = imagem_freq.load()
        all_pixels_img = self.img.load()
        
        matriz_pixels_freq = [ [ None for y in range( imagem_freq.width ) ] for x in range( imagem_freq.height ) ] 

        #LOOPS Q PASSAM POR CADA PIXEL DA FREQ
        for width_freq in range(imagem_freq.width):
            for height_freq in range(imagem_freq.height):
                #pixel_freq = all_pixels_freq[width_freq, height_freq]
                #pixel_freq = np.array([pixel_freq],dtype='float64')
                ##primeiro fazer os somatorios
                somatorio = np.zeros([1,3])
                for width_img in range(self.img.width):
                    for height_img in range(self.img.height):
                        pixel_img = all_pixels_img[width_img,height_img]
                       # pixel_img = np.array([pixel_img],dtype='float64')
                        pixel_img = list(pixel_img)
                        #input("pixel_img: "+str(pixel_img))
                        #NORMALIZA AQUI
                        cos_multiplicados = self.cossenosX[width_img][width_freq] * self.cossenosY[height_img][height_freq]
                        #print("cos_multiplicados: "+str(cos_multiplicados) )
                        #print("cos_multiplicados[0]: "+str(cos_multiplicados[0]) )
                        for c in range(3):
                            pixel_img[c] /= 255
                            #print("somatorio: "+str(somatorio[0]) )
                            #print("cos_multiplicados[0]: "+str(cos_multiplicados[0]) )
                            somatorio[0][c] += pixel_img[c] * cos_multiplicados
                
                somatorio  = self.alfas[width_freq][height_freq] * somatorio 
                #print("somatorio: "+str(somatorio))
                #input("pixel_freq: "+str(pixel_freq))

                #pixel_freq += somatorio
                #matriz_pixels_freq[width_freq][height_freq] = pixel_freq
                matriz_pixels_freq[width_freq][height_freq] = list(somatorio[0])

                #input("pixels da imagem_freq: "+str(matriz_pixels_freq[width,height])) 

        return matriz_pixels_freq

    def alfa(self, pos, size):
        if pos == 0:
            return math.sqrt(1/size) 
        else:
            return math.sqrt(2/size) 

    def matrizParaImagem(self,matriz):
        width, height = len(matriz), len(matriz[0])
        imagem_saida = Image.new('RGB',(width,height))
        pixels_saida = imagem_saida.load()

        for x in range(0,width):
            for y in range(0,height):
                #input("matriz[x][y][0][0]: "+matriz[x][y][0][0])
                pixels_saida[x,y] = (
                    math.floor(self.mapValue(matriz[x][y][0][0], 0, 1, 0, 255)),
                    math.floor(self.mapValue(matriz[x][y][0][1], 0, 1, 0, 255)),
                    math.floor(self.mapValue(matriz[x][y][0][2], 0, 1, 0, 255)),
                )

        return imagem_saida

    def transformadaInversaCossenoDiscreta(self, matriz_img_freq):
        matriz_pixels_imagem_normal = [ [ None for y in range( len(matriz_img_freq[0]) ) ] for x in range( len(matriz_img_freq) ) ] 
        #somatorio = np.zeros([1,3])
        #loop q passa por cada pixel da imagem normal
        #print("len(matriz_img_freq): " +str(len(matriz_img_freq)))
        #print("len(matriz_img_freq[0]): " +str(len(matriz_img_freq[0])))
        #input("matriz_img_freq: "+str(matriz_img_freq))
        for x in range(self.img.width):
            for y in range(self.img.height):
                #pixel_img = matriz_pixels_imagem_normal[x][y]
                somatorio = np.zeros([1,3])
                for u in range(len(matriz_img_freq) ):
                    for v in range(len(matriz_img_freq[0])):
                        #input("matriz_img_freq[u][v]: "+str(matriz_img_freq[u][v]))
                        cos_alfas_multiplicados = self.cossenosX[x][u] * self.cossenosY[y][v] * self.alfas[u][v]
                        for c in range(3):
                            somatorio[0][c] += matriz_img_freq[u][v][c] * cos_alfas_multiplicados
                        #input("somatorio: "+str(somatorio))
                
                #pixel_img = somatorio
                matriz_pixels_imagem_normal[x][y] = somatorio
        
        return matriz_pixels_imagem_normal
    
    def mapValue(self, value, bMin, bMax, nMin, nMax):
        normalized = (value - bMin) / (bMax - bMin)
        return nMin + normalized * (nMax - nMin)

    def calculandoConstantes(self, width, height):
        #print('Calculando constantes...')
        self.alfas = [[ (self.alfa(u, width) * self.alfa(v, height)) for v in range(height)] for u in range(width)]
        self.cossenosX = [[ math.cos(((2 * x + 1) * u * math.pi) / (2 * width)) for u in range(width)] for x in range(width)]
        self.cossenosY = [[ math.cos(((2 * y + 1) * v * math.pi) / (2 * height)) for v in range(height)] for y in range(height)]
    
    def distanciaQuadrada(self, x1, y1, x2, y2):
        dx = x1 - x2
        dy = y1 - y2
        return dx ** 2 + dy ** 2

    def algoritmoDaReta(self, x1, y1, x2, y2):
        x = x1
        y = 0
       #valor = 0
        point_list = []
        a = (y2-y1)/(x2-x1)
        while(x <= x2):
            y = (y1 + (a * (x - x1) ) )
            point_list.append([x,y])
            #self.desenharPixel(x, y)

    def algoritmoDeBresenham(self, x1, y1, x2, y2,xmax,ymax):
        dx  = x2 - x1
        dy  = y2 - y1
        y   = y1 
        eps = 0
        x = x1
        lista_pontos = []
        while( x <= x2 ):
            #self.desenharPixel(x, y)
            lista_pontos.append([x,y])
            eps += dy 
            if( (eps << 1) >= dx ):
                y += 1
                eps -= dx
            x += 1
        return self.desenharLinha(lista_pontos, xmax, ymax)

    def desenharLinha(self, lista_pontos,xmax,ymax):
        matriz_de_zeros = np.zeros([lista_pontos[len(lista_pontos)-1][0], lista_pontos[len(lista_pontos)-1][1]])
        #matriz_de_zeros = [ [0] * lista_pontos[len(lista_pontos)-1][1] ] * lista_pontos[len(lista_pontos)-1][0]
        #print("matriz_de_zeros:" + str(matriz_de_zeros))
        #input("lista_pontos:" + str(lista_pontos))
        for ponto in lista_pontos:
            #print("ponto x:" + str(ponto[0]-1))
            x = ponto[0]-1
            #print("ponto y:" + str(ponto[1]-1))
            y = ponto[1]-1
            #input("matriz_de_zeros_pos:" + str(matriz_de_zeros[3]))
            matriz_de_zeros[x][y] += 1
        #input("matriz_de_zeros:" + str(matriz_de_zeros))
        matriz_de_zeros = list(matriz_de_zeros)
        

        #width, height = len(matriz_de_zeros), len(matriz_de_zeros[0])
        width, height = xmax, ymax
        imagem_saida = Image.new('RGB',(width,height))
        pixels_saida = imagem_saida.load()

        for x in range(0,width):
            for y in range(0,height):
                #input("matriz_de_zeros[x][y][0][0]: "+matriz_de_zeros[x][y][0][0])
                #pixels_saida[x,y] = (
                #    math.floor(self.mapValue(matriz_de_zeros[x][y][0], 0, 1, 0, 255)),
                #    math.floor(self.mapValue(matriz_de_zeros[x][y][1], 0, 1, 0, 255)),
                #    math.floor(self.mapValue(matriz_de_zeros[x][y][2], 0, 1, 0, 255)),
                #)
                #input(matriz_de_zeros[x][y])
                
                pixels_saida[x,y] = (
                    255,
                    255,
                    255
                )

        for x in range(0,len(matriz_de_zeros)):
            for y in range(0, len(matriz_de_zeros[0])):
                if(matriz_de_zeros[x][y] == 1):
                    pixels_saida[x,y] = (
                        1,
                        1,
                        1
                    )
        return imagem_saida
              