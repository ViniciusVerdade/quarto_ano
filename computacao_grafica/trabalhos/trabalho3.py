from PIL import Image
import random
import numpy as np

def matrizConvolucao (img, m):
    new_img = Image.new("RGB", (img.width, img.height))
    new_img_pixels = new_img.load()
    pixels_array = img.load()

    width = len(m[:,0])
    height= len(m[0,:])
    offset_width = int(width/2 - 0.5)
    offset_height = int(height/2 - 0.5) #o que é esse offset?
    for i in range(img.width):
        for j in range(img.height):
            xi = i - offset_width
            xf = i + offset_width
            yi = j - offset_height
            yf = j + offset_height
            if (xi < 0 or yi < 0 or xf >= img.width or yf >= img.height):
                continue
            value = np.zeros([3,1])
            for m in range(xi, xf+1):
                for n in range(yi, yf+1):
                    pixel = pixels_array[m,n]
                    value[0] += pixel[0] * m[m-xi, n-yi]
                    value[1] += pixel[1] * m[m-xi, n-yi]
                    value[2] += pixel[2] * m[m-xi, n-yi]
            new_img_pixels[i,j] = (value[0], value[1], value[2])
    return new_img

img = Image.open("../imagens/eu.jpg")
m = np.ones([3,3])
#[[1,1,1]
# [1,1,1]
# [1,1,1]]
coeficiente = sum(sum(m))
img_with_mask = matrizConvolucao(img,m/coeficiente)
img_with_mask.show()