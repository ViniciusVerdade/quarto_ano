# Python program to read 
# image using PIL module 
  
from PIL import Image
from math import floor, acos, sqrt, degrees
  
# Read image 
img = Image.open('../imagens/a.jpg') 
pixels = img.load()

imgCMYK = Image.new('CMYK',(img.width,img.height))
newPixelsCMYK = imgCMYK.load()
#CMYK 
for width in range(img.width):
    for height in range(img.height):
        pixel = pixels[width,height]
        #RGB VALS
        r = pixel[0]
        g = pixel[1]
        b = pixel[2]
        #CMYK VALS
        c = 1 - (r/255)
        m = 1 - (g/255)
        y = 1 - (b/255)
        k = min(c,m,y)

        c = (c-k)/(1-k)
        c = floor(255*c)

        m = (m-k)/(1-k)
        m = floor(255*m)

        y = (y-k)/(1-k)
        y = floor(255*y)
        k = floor(255*k)

        cmyk = (c,m,y,k)
        newPixelsCMYK[width, height] = cmyk
imgCMYK.show()

    
imgHSV = Image.new('HSV', (img.width, img.height))
newPixelsHSV = imgHSV.load()
for width in range(img.width):
    for height in range(img.height):
        pixel = pixels[width, height]
        r = pixel[0]
        g = pixel[1]
        b = pixel[2]
        r = r/255
        g = g/255
        b = b/255
        cmax = max(r, g, b)
        cmin = min(r, g, b)
        delta = cmax - cmin
        if delta == 0:
            h = 0
        elif cmax == r:
            h = 60 * (((g - b)/delta) % 6)
        elif cmax == g:
            h = 60 * (((b - r)/delta) + 2)
        elif cmax == b:
            h = 60 * (((r - g)/delta) + 4)
        s = 0
        if cmax != 0:
            s = delta/cmax
        i = cmax
        h = floor(255 * (h/360))
        s = floor(255 * s)
        i = floor(255 * i)
        hsi = (h, s, i)
        newPixelsHSV[width, height] = hsi
imgHSV.show()