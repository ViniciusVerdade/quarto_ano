from PIL import Image
from math import floor, acos, sqrt, degrees
import numpy as np
  
# Read image 
img = Image.open('../imagens/eu.jpg') 
pixels = img.load()

hist = np.zeros([256,3])
#HISTOGRAMA
for width in range(img.width):
    for height in range(img.height):
        pixel = pixels[width,height]

        hist[pixel[0],0] += 1
        hist[pixel[1],1] += 1
        hist[pixel[2],2] += 1

hist_normalizado = hist / (img.size[0]*img.size[1])

histogramaAcumulado = np.zeros([256,3])

histogramaAcumulado[0][0] = hist_normalizado[0][0]
histogramaAcumulado[0][1] = hist_normalizado[0][1]
histogramaAcumulado[0][2] = hist_normalizado[0][2]

for j in range(histogramaAcumulado.shape[1]):
    for i in range(1,histogramaAcumulado.shape[0]):
        histogramaAcumulado[i][j] = histogramaAcumulado[i-1][j] + hist_normalizado[i][j]

print(histogramaAcumulado)

imgEqualizada = Image.new('RGB',(img.width,img.height))
pixels_array = imgEqualizada.load()

for width in range(imgEqualizada.width):
    for height in range(imgEqualizada.height):
        p = pixels[width,height]

        r = int(255 * histogramaAcumulado[p[0], 0])#valor do histograma acumulado transformado em R
        g = int(255 * histogramaAcumulado[p[1], 2])#valor do histograma acumulado transformado em G
        b = int(255 * histogramaAcumulado[p[2], 2])#valor do histograma acumulado transformado em B

        pixels_array[width,height] = (r, g, b)

imgEqualizada.show()