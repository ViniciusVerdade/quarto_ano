from PIL import Image
import random
import numpy as np
from ImageHandler import ImageHandler

def main():
    image_handler = ImageHandler("./imagens/50_50.png")
    x1 = 1
    y1 = 1
    x2 = 190
    y2 = 100
    xmax = 200
    ymax = 200
    imagem = image_handler.algoritmoDeBresenham(x1,y1,x2,y2,xmax,ymax)
    imagem.save("./imagens_resultado/linha_"+ str(x1) + "_" + str(y1) + "_" + str(x2) + "_" + str(y2) + ".jpg")
    #image_handler.filtragemButterworthComDCT()
    #image_handler.filtragemLaplace(1)
    #image_handler.filtragemSobel(1)


main()