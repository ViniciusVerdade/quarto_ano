import numpy as np
import random as rand
import pandas as pd
import matplotlib.pyplot as plt

def plotResult(dataset, w, b): ##É NECESSÁRIO ARRUMAR ESSA PASSAGEM

    class_a = dataset[dataset[2] == 1]
    class_b = dataset[dataset[3] == 1]
    class_c = dataset[dataset[4] == 1]
    plt.xlim(-10, 10)
    plt.ylim(-10, 10)

    plt.scatter(class_a[0], class_a[1], color='r', s=10)
    plt.scatter(class_b[0], class_b[1], color='b', s=10)
    plt.scatter(class_c[0], class_c[1], color='g', s=10)

    x = np.linspace(-10, 10, 100)

    slope_a = -(b[0]/w[0][1])/(b[0]/w[0][0])  
    slope_b = -(b[1]/w[1][1])/(b[1]/w[1][0])  
    slope_c = -(b[2]/w[2][1])/(b[2]/w[2][0])  
    intercept_a = -b[0]/w[0][1]
    intercept_b = -b[1]/w[1][1]
    intercept_c = -b[2]/w[2][1]

    #print("x: "+str(x))
    #print("slope_a: "+str(slope_a))
    ya = (x * slope_a) + intercept_a
    yb = (x * slope_b) + intercept_b
    yc = (x * slope_c) + intercept_c

    plt.plot(x, ya, 'r', linestyle="-")
    plt.plot(x, yb, 'b', linestyle="-")
    plt.plot(x, yc, 'g', linestyle="-")

    plt.show()

##
def settingXDThroughFile(filename):
    #f = open(filename, "r")
    data = pd.read_csv(filename, header=None)
    
    return data


##Retorna o valor de y
def equation(x,weights,b):
    #print("w: "+str(weights))
    #print("b: "+str(b))
    #print("x: "+str(x))
    #print("w_shape: "+str(weights.shape))
    #print("x_shape: "+str(x.shape))
    #print("b_shape: "+str(b.shape))
    val = np.sum(weights * x, axis = 1) + b
    #print("val: "+str(val))
    #input()
    return val


##Função que faz a derivada do erro
def derivadaErro(d,y):
    return (y * 2 - d * 2)

##Função de inicialização do Bias e dos pesos
def inicializacao(coords,D):
    
    weights = np.zeros([D.shape[1], len(coords[0])])
    b = []
    for m in range(0,D.shape[1]):
        for n in range(0,len(coords[0])):
            weights[m][n] += rand.uniform(0, 1)
        b.append(rand.uniform(0, 1))
    b = np.array(b)

    #print("w: "+str(weights))
    #print("b: "+str(b))
    #print("w_shape: "+str(weights.shape))
    #print("b_shape: "+str(b.shape))
    #input()
    
    return weights, b

def sigmoid(x):
    return 1/(1 + (np.e**(-x) ) )

def d_sigmoid(sig_x):
    return sig_x * (1 - sig_x)

##Função que faz o treinamento dos neurônios
def treinamento(coords,D,Ep,_lambda):
    weights, b = inicializacao(coords,D)

    for ep in range(0,Ep):
        for x, d in zip(coords,D):
            #print("x: "+ str( x))
            #print("d: "+ str( d))
            y = sigmoid( equation(x,weights,b) ) #retorna vetor, pois é um y para cada classe
            #print("erro: "+ str( (d-y)**2 ))
            #print("y: "+ str( y))
            learned = (_lambda/coords.shape[1]) * derivadaErro(d,y) * d_sigmoid(y) 
            #print("learned: "+ str( learned))
            #input()
            #input()
            b = b - learned

            for n in range(0, D.shape[1]):
                learned_value = learned[n]
                #print("learned_val: "+ str(learned[n]))
                learned_value = learned_value * x #primeiros dois pesos, segundos dois pesos, terceiro dois pesos iterando bonitinho
                #print("learned_val: "+ str(learned_value))
                #input()   
                weights[n] = weights[n] - learned_value

            #weights = weights -  np.array( [ (_lambda/len(coords) * derivadaErro(d,y) * d_sigmoid(y)) ] ).T 

            #for m in range(0,len(d)) :##len(d) é o númedo de CAMADAS
            #for n in range(0,len(coords[0])):##len(d) é o númedo de coordenadas
            #    learned = ( (1/len(coords))*_lambda * derivadaErro(d,y) * d_sigmoid(y) * x[n])
            #    weights[n] = weights[n] - learned
            #b = b - ( (1/len(coords))*_lambda * derivadaErro(d,y) * d_sigmoid(y))

    #weights = np.transpose(weights)
    return weights, b


##Função que faz a predição para definição da precisão e do recal
def predicao(x, w, b,_class):
    #print(w[_class])
    p = sum(w[_class] * x.T) + b[_class]
    if np.tanh(p) > 0:
        return 1
    else:
        return -1
##Função que checa a precisão e o recall relacionado a acertos e erros ##FAZER DE TODOS JUNTOS
def precisionRecall (coords,D,weights,bias,_class):
    vp = 0
    fp = 0
    fn = 0

    for x, d in zip(coords,D[_class]):
        val_predict = predicao(x,weights,bias,_class)
        if val_predict == d:
            if val_predict == 1:
                vp += 1
        else:
            if val_predict == 1:
                fp += 1
            elif val_predict == -1:
                fn += 1
    precision = vp / (vp + fp)
    recall   = vp / (vp + fn)
    print("\nprecision from class "+str(_class)+": " + str(precision))
    print("recal from class "+str(_class)+": " + str(recall))

##Função de inicialização da base de dados
def inicializacaoArquivo(filename):
    #filename = 'facil_treinamento.csv'
    data = settingXDThroughFile(filename)
    coords = np.stack((data[0], data[1]), 1 )
    D = []
    #print(len(data.T))
    for n in range(2,len(data.T)):
        D.append(data[n])

    return coords, D, data
##Função Main
def main():
    coords, D, data_treino = inicializacaoArquivo('facil_treinamento.csv')
    D = np.transpose(np.array(D))
    #print("D: "+str(D[:3]))
    #print("coords: "+str(coords[:5]))
    #input()

    weights, bias = treinamento(coords, D, 100, 0.2)
    print("weigths: " + str(weights))
    print("bias: " + str(bias))
    coords_teste, D_teste, data_teste = inicializacaoArquivo('facil_teste.csv')

    precisionRecall(coords_teste,D_teste,weights,bias,0)
    precisionRecall(coords_teste,D_teste,weights,bias,1)
    precisionRecall(coords_teste,D_teste,weights,bias,2)
                
    plotResult(data_treino, weights, bias)
    

    

    
main()