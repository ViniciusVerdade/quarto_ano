import numpy as np
import random as rand
import pandas as pd
import matplotlib.pyplot as plt
import utils as utils

def perceptrons(coords, w, b):
    y = sigmoid( np.sum(w * coords, axis=1) + b )
    y[y == y.max()] = 1
    y[y != 1] = 0
    return y

def plot_result(dataset, coords, labels, w, b):
    n_classes = labels.shape[1]
    
    plt.xlim(-15, 15)
    plt.ylim(-15, 15)

    # Para cada classe no dataset
    # Plotar pontos
    for _class in range(2, dataset.shape[1]):
        data = dataset[ dataset[_class] == 1 ]
        plt.scatter(data[0], data[1], color=utils.Color(_class).name, s=10)


    # Plotar linhas
    x = np.linspace(-15, 15, 100)
    for _class, wx, bx in zip(range(2, dataset.shape[1]), w, b):
        slope = -(bx/wx[1])/(bx/wx[0])
        intercept = -bx/wx[1]
        y = slope * x + intercept
        plt.plot(x, y, utils.Color(_class).name)


    # Relatório
    true_positive = np.zeros(n_classes)
    true_negative = np.zeros(n_classes)
    false_positive = np.zeros(n_classes)
    false_negative = np.zeros(n_classes)

    for coord, label in zip(coords, labels):
        y = perceptrons(coord, w, b)

        if (y == label).all():
            true_positive += label
            label[label == 1] = 2
            label[label == 0] = 1
            label[label == 2] = 0
            true_negative += label

        else:
            classe_correta = np.argmax(label)
            predict_errado = np.argmax(y)

            false_positive[predict_errado] += 1

            for i in range(n_classes):
                if i != classe_correta and i != predict_errado:
                    true_negative[i] += 1

            false_negative[classe_correta] += 1

    print('Verdadeiro Positivo: ', true_positive)
    print('Falso Positivo: ', false_positive)
    print('Verdadeiro Negativo: ', true_negative)
    print('Falso Negativo: ', false_negative)
    precision = true_positive / (true_positive + false_negative)
    reccal = true_positive / (true_positive + false_negative)

    print('Precisão:  ', precision)
    print('Revocação: ', reccal)


    plt.show()


def importarDados(filename):
    dataset = pd.read_csv(filename,header=None)
    distancias = np.stack((dataset[0], dataset[1], dataset[2], dataset[3]), 1 )
    #input(str(distancias.shape) )
    respostas = np.delete(np.array(dataset),[0,1,2,3], axis=1)
    return dataset, distancias, respostas

def sigmoid(x):
    return 1/ (1 + np.exp(-x))

def derivadaSigmoid(sigmoid):
    return sigmoid * ( 1 - sigmoid)

def derivadaErro(d,y):
    return 2 * y - 2 * d

def treinamento(distancias, respostas, ep = 30, _lambda = 0.2, num_camadas = 1):
    nPesos = len(distancias[0])
    nClasses = len(respostas[0])
    nCamadas = 1

    pesos = np.random.random((nClasses,nPesos))#matriz de inicialização com nClasses x nPesos
    bias = np.random.random((nClasses))# um bias por classe

    input(distancias.shape)
    for e in range(0,ep):
        for x, resp_esperada in zip(distancias, respostas):
            
            #y = np.array([num_camadas])
            y = []
            i = 0
            #propagação
            while(i < num_camadas):
                if(i == 0):
                    y.append( sigmoid( np.sum( pesos * x , axis = 1) + bias) )
                else:
                    y.append( sigmoid( np.sum( pesos * y[i-1] , axis = 1) + bias) )
                i = i + 1
            
            print("pesos.shape: " + str(pesos.shape))
            print("x.shape: " + str(x.shape))
            print("b.shape" + str(bias.shape))
            input("y.shape" + str(y))
            #propagação reversa
            x_matriz = np.array([x])
            learned = (_lambda/len(distancias) * derivadaErro(resp_esperada,y) * derivadaSigmoid(y))
            learned_matriz = np.array([learned])
            pesos = pesos - np.matmul(learned_matriz.T, x_matriz)
            bias  = bias - learned
            #print("bshape" + str(bias.shape))
            #print("xshape" + str(x.shape))

    return pesos, bias

def main():
    filename = "./data/iris_treinamento.csv"
    #filename_teste = "iris_teste.csv"
    dataset, distancias,respostas = importarDados(filename)

    num_camadas = 3    
    pesos, bias = treinamento(distancias, respostas, 300, 0.2, num_camadas)
    print("pesos: " + str(pesos))
    print("bias: " + str(bias))
    
    plot_result(dataset,distancias,respostas, pesos, bias)

main()