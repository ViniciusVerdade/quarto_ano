from enum import Enum

class Color(Enum):
    k = 2
    b = 3
    y = 4
    g = 5
    r = 6