import numpy as np
import random as rand
import pandas as pd
import matplotlib.pyplot as plt
import utils as utils
from RedeNeural import RedeNeural

def main():
    #filename_treino = "./data/iris_treinamento.csv"
    #filename_validacao = "./data/iris_validacao.csv"
    #filename_teste = "./data/iris_teste.csv"
    rna = RedeNeural(4,3,[])
    train_entrada, train_valor_esperado, val_entrada, val_valor_esperado, test_entrada, test_valor_esperado = rna.preparandoData()
    rna.treinandoDataset(train_entrada, train_valor_esperado, val_entrada, val_valor_esperado)
    rna.testandoTreinamento(test_entrada, test_valor_esperado)

    #print("Pesos: "+ str(pesos))
    #input("Bias: "+ str(bias))
    rna.printAllPesosBias()
main()