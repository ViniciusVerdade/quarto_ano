from Camada import Camada
import pandas as pd
import numpy as np
import random as rand
import constants

class RedeNeural:
    def __init__(self, n_entrada, n_saida, camadas_ocultas = []):
        self.n_entrada = n_entrada #numero de valores de entrada
        self.n_saida = n_saida #numero de classes
        self.epocas = constants.EPOCA
        self.taxa_aprendizado = constants.LAMBDA
        self.tamanho_dataset = 0
        self.pesos = None
        self.bias = None
        self.camadas = self.initCamadas(camadas_ocultas)
    
    def initCamadas(self, camadas_ocultas): 
        """
        Cada posicao de camadas_ocultas possui o numero de neuronios da camada
        """
        camadas = [None] * (len(camadas_ocultas) + 1) #numero de camadas 
        #input(camadas)
        #camadas_ocultas = [n_neuronios_1_camada, n_neuronios_2_camada, ...]
        if len(camadas) == 1:
            camadas[0] = Camada(self.n_entrada, self.n_saida)
            return camadas
        else:
            camadas[0] = Camada(self.n_entrada, camadas_ocultas[0])
        
        i = 1
        for camada in camadas_ocultas:
            if(len(camadas_ocultas) == i):
                camadas[i] = Camada(camada, self.n_saida)
                break
            else:
                camadas[i] = Camada(camada, camadas_ocultas[i])
            i += 1
        
        return camadas
           
    
    def treinandoDataset(self, train_entrada, train_valor_esperado, val_entrada, val_valor_esperado):
        #dataset = pd.read_csv(filename,header=None)
        #dataset = np.genfromtxt(filename_treino,delimiter=",")
        #train_entrada, train_valor_esperado, val_entrada, val_valor_esperado, test_entrada, test_valor_esperado = self.preparandoData()
        #print(dataset)
        self.tamanho_dataset = train_entrada.shape[0]
        #input("tamanho_dataset:" + str(self.tamanho_dataset))
        for e in range(0, self.epocas):
            for entrada, valor_esperado in zip(train_entrada,train_valor_esperado):
                #entrada, valor_esperado = self.formatandoLinha(data)
                #input("Entrada: "+ str(entrada) + " Valor esperado: "+str(valor_esperado))
                #print("self.entrada: "+str(entrada))
                #print("self.valor_esperado: "+str(valor_esperado))
                entrada = np.array([entrada])
                self.treinamento(entrada, valor_esperado)
                #print("\n\n**** TERMINEI UMA LINHA ****\n")
                #self.atualizaEntrada()


    def treinamento(self, entrada, valor_esperado):
        """
        Entrada e valor_esperado é a linha do csv dividido
        """
        #forward propagation
        #h = self.predicao(entrada)
        #print("#### DEBUG PREDIÇÃO ###")
        self.predicao(entrada)
        #calculo dos pesos e bias
        #print("#### DEBUG PROPAGACAO REVERSA ###")
        pesos, bias = self.propagacaoReversa(valor_esperado)

        return pesos, bias

    def propagacaoReversa(self, valor_esperado):
        valor_anterior = None
        peso_anterior = None
        valor_esperado = np.array([valor_esperado])
        for camada in reversed(self.camadas): #ver se o reversed funfa
            
            #input("valor_anterior: "+str(valor_anterior))
            #input("\nvalor_esperado.shape" + str(valor_esperado.shape))
            valor_anterior = camada.delta(valor_esperado,peso_anterior,valor_anterior)
            pesos = camada.atualizaPesos(self.tamanho_dataset)
            bias = camada.atualizaBias(self.tamanho_dataset)
            
            peso_anterior = camada.getPesos()

            
        return pesos, bias
        

    def formatandoLinha(self, linha):
        """
        Recebe uma linha do arquivo de entrada \n
        Retorna uma array com os valores da entrada e uma array com os valores do resultado esperado
        """
        distancias = np.zeros([self.n_entrada, 1])
        classes = np.zeros([self.n_saida, 1])
        #input("distancias: " + str(distancias))
        i = 0
        for l in linha:
            if i < self.n_entrada:
                distancias[i] += l
            else:
                classes[i - self.n_entrada] += l
            i += 1
        return distancias, classes    
    
    def arredonda(self, valor_obtido):
        #return (valor_obtido == np.max(valor_obtido)[:,None]).astype(int)
        _max = np.max(valor_obtido)
        valor_obtido[valor_obtido == _max] = 1
        valor_obtido[valor_obtido != 1] = 0
        return valor_obtido

    def testandoTreinamento(self, test_entrada, test_valor_esperado):
        #dataset = np.genfromtxt(filename,delimiter=",")
    
        true_positive = np.zeros([self.n_saida])
        true_negative = np.zeros([self.n_saida])
        false_negative = np.zeros([self.n_saida])
        false_positive = np.zeros([self.n_saida])
        
        for entrada, valor_esperado in zip(test_entrada, test_valor_esperado):
            #entrada, valor_esperado = self.formatandoLinha(data)
            entrada = np.array([entrada])
            valor_esperado = np.array([valor_esperado])
            valor_obtido = self.predicao(entrada)
            valor_obtido = self.arredonda(valor_obtido)
            #print("entrada: "+str(entrada))
            #print("valor_obtido: "+str(valor_obtido) )
            #print("valor_esperado: "+str(valor_esperado) )

            if (valor_esperado == valor_obtido).all():
                #print("true_positive: "+str(true_positive))
                #print("valor_esperado.T: "+str(valor_esperado.T))
                valor_esperado = np.transpose(valor_esperado)[0]
                true_positive += valor_esperado
                aux = valor_obtido.copy()
                aux[aux == 1] = 2
                aux[aux == 0] = 1
                aux[aux == 2] = 0
                aux = np.transpose(aux)[0]
                true_negative += aux
            else:
                
                classe_correta = np.argmax(valor_esperado)
                predict_errado = np.argmax(valor_obtido)

                false_positive[predict_errado] += 1
                for i in range(self.n_saida):
                    if i != classe_correta and i != predict_errado:
                        true_negative[i] += 1
                false_negative[classe_correta] += 1
        
        #if(true_positive>0).all():
        f1 = 0
        precision = true_positive / (true_positive + false_positive)
        reccal = true_positive / (true_positive + false_negative)
        accuracy = (true_positive + true_negative)/(true_positive + true_negative + false_negative + false_positive)
        f1 = 2 * (precision * reccal) / (precision + reccal)
        print("> precision: "+str(precision))
        print("> reccal: "+str(reccal))
        print("> accuracy: "+str(accuracy))
        print("> f1: "+str(f1))
        #else:
        #    print("Puts! O seu algoritmo errou tudo! Pelo visto não é tão INTELIGENTE assim... :/")
    
    def preparandoData(self):

        # Ler dataset do arquivo
        dataset = np.genfromtxt('./data/iris.csv', delimiter=',')

        # Separar classes
        setosa = dataset[dataset[:, 4] == 1]
        versicolor = dataset[dataset[:, 5] == 1]
        virginica = dataset[dataset[:, 6] == 1]

        # Embaralhar
        np.random.shuffle(setosa)
        np.random.shuffle(versicolor)
        np.random.shuffle(virginica)

        # Separar conjuntos de treinamento, validação e teste
        train = np.concatenate((setosa[:30], versicolor[:30], virginica[:30]))
        val = np.concatenate((setosa[30:40], versicolor[30:40], virginica[30:40]))
        test = np.concatenate((setosa[40:50], versicolor[40:50], virginica[40:50]))


        # Embaralhar após separação
        np.random.shuffle(train)
        np.random.shuffle(val)
        np.random.shuffle(test)
        
        # Separar x e y
        train_entrada,    train_valor_esperado   =  train[:, :4], train[:, 4:]
        val_entrada,      val_valor_esperado     =  val[:, :4],   val[:, 4:]
        test_entrada,     test_valor_esperado    =  test[:, :4],  test[:, 4:]


        return train_entrada, train_valor_esperado, val_entrada, val_valor_esperado, test_entrada, test_valor_esperado

    def predicao(self, saida_camada_anterior, pesos = None, bias = None):
        for camada in self.camadas:
            saida_camada_anterior = camada.predicao(saida_camada_anterior)
        return saida_camada_anterior #esse é o valor do y
    
    def printAllPesosBias(self):
        i = 1
        for camada in self.camadas:
            print("Camada %i:" % i)
            camada.printPesosBias()
            print("\n")
            i += 1