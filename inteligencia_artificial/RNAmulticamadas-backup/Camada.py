import numpy as np
import constants

class Camada:
    def __init__(self, n_entrada, n_neuronios):
        self.n_entrada = n_entrada
        self.n_neuronios = n_neuronios
        self.entrada = None
        self.valor_predito = None
        self.valor_aprendido = None
        self.pesos = self.initPesos(n_neuronios, n_entrada)
        self.bias = self.initBias(n_neuronios)
    
    def initPesos(self, n_neuronios, n_entrada):
        pesos = np.random.random( (n_neuronios,n_entrada) )#matriz de inicialização com nClasses x nPesos
        return pesos

    def initBias(self, n_neuronios):
        bias = np.random.random([n_neuronios,1])
        return bias

    def ativacao(self, x):
        """
        A função de ativação é utilizada para encontrar o y, neste caso, o sigmoide
        """
        return 1/ (1 + np.exp(-x))

    
    def predicao(self, saida_camada_anterior):
        self.entrada = saida_camada_anterior
        #print("\npesos"+str(self.pesos))
        #print("saida_camada_anterior"+str(saida_camada_anterior))
        #print("bias"+str(self.bias))
        #print("bias.shape: "+str(self.bias.shape))
        self.valor_predito = self.ativacao( np.matmul( self.pesos, saida_camada_anterior.T ) + self.bias) ## ALERTA TALVEZ ESTEJA ERRADO
        #input("valor_predito"+str(self.valor_predito))
        return self.valor_predito.T #valor do h da camada
    
    def delta(self, valor_esperado, peso_anterior = None, valor_aprendido_anteriormente = None): #MUDAR 1 POR QUANTIDADE DE ELEMENTOS NA BASE DE DADOS
        if (valor_aprendido_anteriormente is None) and (peso_anterior is None):
            #print("\nvalor_esperado: "+str(valor_esperado))
           # print("valor_predito: "+str(self.valor_predito))
            #print("valor_esperado.shape: "+str(valor_esperado.shape))
            #print("valor_predito.shape: "+str(self.valor_predito.shape))
            self.valor_aprendido = self.derivadaErro(valor_esperado, self.valor_predito.T) * self.derivadaSigmoid(self.valor_predito.T)
            #input("valor_aprendido: "+str(self.valor_aprendido))
        else:#acredito que terei de fazer a transposta do peso_anterior
            #print("valor_aprendido: " + str(valor_aprendido_anteriormente))
            #print("valor_predito: "+str(self.valor_predito))
            #input("peso_anterior: " + str(peso_anterior))
            
            self.valor_aprendido = np.matmul( peso_anterior.T, valor_aprendido_anteriormente.T) *  self.derivadaSigmoid(self.valor_predito) #talvez esteja errado
            self.valor_aprendido = np.transpose(self.valor_aprendido)
        return self.valor_aprendido

    
    def derivadaSigmoid(self, sigmoid):
        return sigmoid * ( 1 - sigmoid)

    def derivadaErro(self, d, y):
        return 2 * y - 2 * d


    def atualizaPesos(self, tamanho_dataset): # trocar esse 1 pelo tamanho da base // colocar a transposta da entrada
        #print("\nvalor_aprendido: "+str(self.valor_aprendido))
        #print("self.entrada: "+str(self.entrada))
        #print("valor_aprendido.shape: "+str(self.valor_aprendido.shape))
        #print("self.entrada.shape: "+str(self.entrada.shape))
        #input("self.pesos.shape: "+str(self.pesos.shape))

        self.pesos = self.pesos - (constants.LAMBDA/ tamanho_dataset * np.matmul(self.valor_aprendido.T , self.entrada) )
        #print("self.pesos: "+str(self.pesos))
        return self.pesos

    def atualizaBias(self, tamanho_dataset): # trocar esse 1 pelo tamanho da base
        #print("self.bias: "+str(self.bias))
        self.bias = self.bias - (constants.LAMBDA/ tamanho_dataset * self.valor_aprendido.T )
        #print("self.bias: "+str(self.bias))
        return self.bias

    def printPesosBias(self):
        print("Pesos da camada com "+str(self.n_entrada)+" entrada e "+str(self.n_neuronios)+" neuronios:")
        print(str(self.pesos))
        print("Bias: ")
        print(str(self.bias))
    
    def getPesos(self):
        return self.pesos