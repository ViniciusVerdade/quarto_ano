import numpy as np
import random as rand
import pandas as pd
import matplotlib.pyplot as plt

def plot_result(dataset, w, b):
    class_a = dataset[dataset[2] ==  1]
    class_b = dataset[dataset[2] == -1]
    plt.xlim(-5, 5)
    plt.ylim(-5, 5)

    plt.scatter(class_a[0], class_a[1], color='r', s=10)
    plt.scatter(class_b[0], class_b[1], color='b', s=10)

    x = np.linspace(-5, 5, 100)

    slope = -(b/w[1])/(b/w[0])  
    intercept = -b/w[1]

    y = (x * slope) + intercept

    plt.plot(x, y, 'g')

    plt.show()

def settingXDThroughFile(filename):
    #f = open(filename, "r")
    data = pd.read_csv(filename, header=None)
    
    return data

def equation(x,weigths,b):
    val = np.sum(weigths * x.T) + b
    return val

def derivada_erro(d,y):
    return (2*y - 2*d)

def treinamento(coords,D,Ep,_lambda):
    weights = []
    for n in range(0,len(coords[0])):
        weights.append(rand.uniform(-1, 1))
    b = rand.uniform(-1, 1)

    for ep in range(0,Ep):

        for x, d in zip(coords,D):
              
            y = np.tanh( equation(x,weights,b) )
            for n in range(0,len(coords[0])):
                weights[n] = weights[n] - ( (1/len(coords))*_lambda * derivada_erro(d,y) * x[n])
            
            b = b - ( (1/len(coords))*_lambda * derivada_erro(d,y))

    return weights, b
            
def predicao(x, w, b):
    p = sum(x.T * w) + b
    if np.tanh(p) > 0:
        return 1
    else:
        return -1

def main():
    filename = 'data.csv'
    data = settingXDThroughFile(filename)
    coords = np.stack((data[0], data[1]), 1 )
    D = data[2]

    weights, bias = treinamento(coords, D, 20, 0.2)
    print("weigths: " + str(weights))
    print("bias: " + str(bias))

    vp = 0
    fp = 0
    fn = 0

    for x, d in zip(coords,D):
        val_predict = predicao(x,weights,bias)
        if val_predict == d:
            if val_predict == 1:
                vp += 1
        else:
            if val_predict == 1:
                fp += 1
            elif val_predict == -1:
                fn += 1
    precision = vp / (vp + fp)
    recall   = vp / (vp + fn)
    print("precision: " + str(precision))
    print("recal: " + str(recall))

                
    plot_result(data, weights, bias)
    

    

    
main()